package booking_movies_tickets;

//detail of round
import java.util.HashMap;

public class Round {
    private String time; //for show in table
    private Movie movie;
    private Theater theater;
    private String nameTheater; //for show in table
    private String detail; //for show in table
    private HashMap<String,Seat> seatInTheater;


    public Round(String time, Movie movie, Theater theater) {
        this.time = time;
        this.movie = movie;
        this.theater = theater;
        nameTheater=theater.getName();
        seatInTheater=new HashMap<>();
        creatSeat();

        detail=theater.getSystem()+"    "+theater.getLanguage();
        for(Seat s: theater.getTypeSeats()){
            detail+="\n"+s.getTypeOfSeat();
        }
    }

    public String getTime() {
        return time;
    }

    public String getNameTheater() {
        return nameTheater;
    }

    public Movie getMovie() {
        return movie;
    }

    public Theater getTheater() {
        return theater;
    }

    public String getDetail() {
        return detail;
    }

    public HashMap<String, Seat> getSeatInTheater() {
        return seatInTheater;
    }

    public void creatSeat(){
        for (int i=0;i<14;i++){
            for(int j=0;j<18;j++){
                if(theater.getTypeSeats().length>1 && i>=11 ){
                    seatInTheater.put(""+(char)(78-i)+(j+1),new Seat(theater.getTypeSeats()[1],""+(char)(78-i)+(j+1)));
                }
                else {seatInTheater.put(""+(char)(78-i)+(j+1),new Seat(theater.getTypeSeats()[0],""+(char)(78-i)+(j+1)));}
            }}
    }





}
