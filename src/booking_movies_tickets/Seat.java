package booking_movies_tickets;

//detail about seat
public class Seat {
    private String name ;
    private String typeOfSeat;
    private double price;
    private String seatImgpath;
    private Account owner;

    public Seat(String typeOfSeat, double price, String seatImgpath) {
        this.typeOfSeat=typeOfSeat;
        this.price = price;
        this.seatImgpath = seatImgpath;
        name="";
    }
    public Seat(Seat s,String name){
        this.typeOfSeat=s.typeOfSeat;
        this.price=s.price;
        this.seatImgpath=s.seatImgpath;
        this.name=name;
        owner=null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getSeatImgpath() {
        return seatImgpath;
    }

    public Account getOwner() {
        return owner;
    }

    public String getTypeOfSeat() {
        return typeOfSeat;
    }

}
