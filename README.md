# Movie Ticket Booking System - 6110402753 #


### What is this repository for? ###
This is a project for Software Construction (01418211) at Kasetsart University purpose to simulation a movie ticket booking system

### How do I open? ###
* Clone the repository 
##### git clone https://6110402753@bitbucket.org/6110402753/movie-ticket-booking-system-project.git
* Open it in IntelliJ IDEA


### Languages ###
* JavaFx 
* Java 

### Features ###
* Change program theme 
* Sign up as customer
* Can view trailer from each movie 
* login by account that you used to sign up
* booking/canceling seat 
* showing all seat that booked
* save bill as picture.png

### Launch ###
* Run program by .jar file in 6110402753/out/artifacts/6110402753_jar
* Save file requirement
  * Folder named "saveFile"
    * File named "AccountList.csv"
    * File named "BookingData.csv"
    * File named "tmp.csv"
  * Folder named "ScreenShot"
* **If you don't have this save file requirement when run program it will be create for you**

### Author ##
Natnicha Khongsoontorn 


