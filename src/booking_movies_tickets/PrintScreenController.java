package booking_movies_tickets;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import java.time.LocalDateTime;

import java.time.format.DateTimeFormatter;

public class PrintScreenController {
    @FXML
    Label cineplexLabel,movieLabel,timeLabel,theaterLabel,seatLabel,ownerLabel,successLabel;
    @FXML
    HBox page;

    Round round;
    String seatNo,summary;
    ManageAccount manageAccount;
    @FXML
    Button saveBtn,mainBtn,exitBtn,outBtn;

    @FXML
    public void initialize(){
        //set all detail about round that user booking
        page.getStylesheets().add(CssTheme.path);
        manageAccount=ManageAccount.getInstance();
        cineplexLabel.setText("Fabulous Cinema");
        Platform.runLater(() -> {
        movieLabel.setText(round.getMovie().getName());
        theaterLabel.setText(round.getTheater().getName());
        timeLabel.setText("Time: "+round.getTime());
        seatLabel.setText(seatNo);
        ownerLabel.setText("Name:  "+manageAccount.getCurrentCustomers().getName()+"\n"
                +"Surname:  "+manageAccount.getCurrentCustomers().getSurname()+"\n"
                + "Price:  "+summary);
        });
    }

    //Action on Save picture button
    @FXML
    public void saveScreenBtn(ActionEvent e){
        //set invisible all button
        successLabel.setVisible(false);
        saveBtn.setVisible(false);
        mainBtn.setVisible(false);
        exitBtn.setVisible(false);
        outBtn.setVisible(false);

        WritableImage image = page.snapshot(new SnapshotParameters(), null);

        //named picture
        DateTimeFormatter myFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
        LocalDateTime myDateTime = LocalDateTime.now();

        String formattedDate = myDateTime.format(myFormat);

        File dir = new File("ScreenShot");
        if(!dir.exists()){
            dir.mkdirs();
        }
        File file = new File("ScreenShot/"+formattedDate+".png");

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException oe) {

        }
        //set visible all button
        successLabel.setVisible(true);
        saveBtn.setVisible(true);
        mainBtn.setVisible(true);
        exitBtn.setVisible(true);
        outBtn.setVisible(true);
    }

    // data from previous scene (Select seat)
    public void passRoundSeat(Round r,String seat,String sum){
        round=r;
        seatNo=seat;
        summary=sum;
    }

    //Action on Exit button
    //close program
    @FXML
    public void exitBtnAction(ActionEvent e) {
        exitBtn.getScene().getWindow().hide();
    }


    //Action on MainMenu button
    //back to Mainmenu scene
    @FXML
    public void backBtnAction(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/MainMenu.fxml"));

        stage.setScene(new Scene(loader.load(), 1000, 990));


        stage.show();
    }

    //Action on Logout button
    //back to login scene
    @FXML
    public void logoutBtnHandle(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/Login.fxml"));
        stage.setScene(new Scene(loader.load(), 982, 990));

        stage.show();
    }



}
