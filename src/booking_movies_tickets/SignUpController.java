package booking_movies_tickets;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SignUpController {
    private ManageAccount manageAccount;
    @FXML
    TextField userText,passText,nameField,mailField,surField;
    @FXML
    AnchorPane anchor;

    @FXML
    public void initialize(){
        //set css Stylesheets
        //create singleton object
        manageAccount=ManageAccount.getInstance();
        anchor.getStylesheets().add(CssTheme.path);
    }

    @FXML
    public void createAccountBtn(ActionEvent e) throws IOException {
        if(manageAccount.haveAccount(userText.getText())){
            Alert alert =new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("This username is already exists. Please use a different username");
            alert.showAndWait();
        }
        else if(userText.getText().equals("") || passText.getText().equals("") ||nameField.getText().equals("") ||surField.getText().equals("") ||mailField.getText().equals("")){
            Alert alert =new Alert(Alert.AlertType.WARNING);
            alert.setTitle("WARNING");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all box");
            alert.showAndWait();
        }
        else {
            if (userText.getText().contains(",") || passText.getText().contains(",") || nameField.getText().contains(",") || surField.getText().contains(",") || mailField.getText().contains(",")) {
                Alert alert =new Alert(Alert.AlertType.WARNING);
                alert.setTitle("WARNING");
                alert.setHeaderText(null);
                alert.setContentText("every fill box can't contain Comma(,)");
                alert.showAndWait();
            } else {

                Account account = new Account(userText.getText(), passText.getText(), nameField.getText(), surField.getText(), mailField.getText());
                manageAccount.addAccount(account);

                File dir = new File("saveFile");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File file = new File("saveFile/AccountList.csv");
                try {
                    file.createNewFile();
                    FileWriter fileWriter = new FileWriter(file, true);
                    BufferedWriter writer = new BufferedWriter(fileWriter);

                    writer.write(userText.getText());
                    writer.append(",");
                    writer.write(passText.getText());
                    writer.append(",");
                    writer.write(nameField.getText());
                    writer.append(",");
                    writer.write(surField.getText());
                    writer.append(",");
                    writer.write(mailField.getText());
                    writer.newLine();

                    writer.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                backBtnHandle(e);
            }
        }
    }

    @FXML
    public void backBtnHandle(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/Login.fxml"));
        stage.setScene(new Scene(loader.load(), 982, 990));

        stage.show();
    }


}
