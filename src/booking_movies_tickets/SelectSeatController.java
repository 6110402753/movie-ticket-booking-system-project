package booking_movies_tickets;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

public class SelectSeatController {
    @FXML
    GridPane seatGrid;



    private Button[][] btn = new Button[18][14];
    private Label[] rowLabel = new Label[14];
    private ArrayList<String> selectedSeat;
    private ArrayList<String> selectedOwnSeat;
    private Double summary=0.0;
    private ManageAccount manageAccount;
    private AllRound allRound;

    @FXML
    Button backBtn, exitBtn,buyBtn,removeBtn;

    @FXML
    Label labelTheater, labelSelect, labelSummary, nameLabel;
    @FXML
    ImageView seatImg1, seatImg2;
    @FXML
    Label detailSeat1, detailSeat2;
    @FXML
    AnchorPane anchorLarge;


    @FXML
    public void initialize() {
        anchorLarge.getStylesheets().add(CssTheme.path);
        removeBtn.setDisable(true);
        allRound=AllRound.getInstance();
        selectedSeat = new ArrayList<>();
        selectedOwnSeat=new ArrayList<>();
        manageAccount=ManageAccount.getInstance();
        Platform.runLater(() -> {
            // set movie's detail
            nameLabel.setText(allRound.getCurrentRound().getNameTheater());
            labelTheater.setText(allRound.getCurrentRound().getMovie().toString() + "\n" + "Showtime: " + allRound.getCurrentRound().getTime() + "\n" + "System: " + allRound.getCurrentRound().getTheater().getSystem() + "  language: " + allRound.getCurrentRound().getTheater().getLanguage());
            seatImg1.setImage(new Image("/poster_images/seat.png"));
            //set seat detail
            detailSeat1.setText("Regular Seat" + "\n" + allRound.getCurrentRound().getTheater().getTypeSeats()[0].getPrice()+" Bath");
            if (allRound.getCurrentRound().getTheater().getSeatPattern().equals("Mix") ){
                seatImg2.setImage(new Image("/poster_images/seatVip.png"));
                detailSeat2.setText("VIP seat" + "\n" +allRound.getCurrentRound().getTheater().getTypeSeats()[1].getPrice()+ " Bath");
            }
            //creat seat from button
            for (int i = 0; i < 14; i++) {
                for (int j = 0; j < 19; j++) {

                    if (j == 18) {
                        //row name
                        rowLabel[i] = new Label("  " + (char) (78 - i) + " ");
                        rowLabel[i].setStyle("-fx-font-size: 25;-fx-alignment: CENTER;-fx-font-weight: bold;");

                        seatGrid.add(rowLabel[i], j, i, 1, 1);
                    } else {
                        btn[j][i] = new Button();

                        btn[j][i].setMaxSize(48, 40);
                        //65+14-1
                        btn[j][i].setId("" + (char) (78 - i) + (j + 1));
                        //vip seat
                        if (allRound.getCurrentRound().getTheater().getSeatPattern().equals("Mix") && i >= 11) {

                            if(allRound.getCurrentRound().getSeatInTheater().get("" + (char) (78 - i) + (j + 1)).getOwner()!=null){
                                //seat that booked by current user (vip seat)
                                if(allRound.getCurrentRound().getSeatInTheater().get("" + (char) (78 - i) + (j + 1)).getOwner().equals(manageAccount.getCurrentCustomers())){
                                    btn[j][i].setStyle("-fx-background-image: url('/poster_images/seatVip_currentowner.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
                                }
                                //seat that booked by other user (vip seat)
                                else{
                                    btn[j][i].setStyle("-fx-background-image: url('/poster_images/seatVip_otherowner.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
                                }
                            }
                            //seat not booked yet (vip seat)
                            else
                            btn[j][i].setStyle("-fx-background-image: url('/poster_images/seatVip.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");

                        }
                        //regular seat
                        else {
                            if(allRound.getCurrentRound().getSeatInTheater().get("" + (char) (78 - i) + (j + 1)).getOwner()!=null){
                                //seat that booked by current user (regular seat)
                                if(allRound.getCurrentRound().getSeatInTheater().get("" + (char) (78 - i) + (j + 1)).getOwner().equals(manageAccount.getCurrentCustomers())){
                                    btn[j][i].setStyle("-fx-background-image: url('/poster_images/seat_currentowner.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
                                }
                                //seat that booked by other user (regular seat)
                                else{
                                    btn[j][i].setStyle("-fx-background-image: url('/poster_images/seat_otherowner.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
                                }
                            }
                            //seat not booked yet (regular seat)
                            else
                                btn[j][i].setStyle("-fx-background-image: url('/poster_images/seat.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
                        }

                        //set Action and add on gridpane
                        btn[j][i].setOnAction(this::selectBtnAction);
                        seatGrid.add(btn[j][i], j, i, 1, 1);
                    }
                }
            }
        });

    }


    //Action on button seat
    @FXML
    public void selectBtnAction(ActionEvent e) {
        Button b = (Button) e.getSource();

        if(allRound.getCurrentRound().getSeatInTheater().get(b.getId()).getOwner()==null) {
            //remove check
            if (selectedSeat.contains(b.getId())) {
                if ((b.getId().charAt(0) == 'A' || b.getId().charAt(0) == 'B' || b.getId().charAt(0) == 'C') && allRound.getCurrentRound().getTheater().getSeatPattern().equals("Mix"))
                    b.setStyle("-fx-background-image: url('/poster_images/seatVip.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
                else
                    b.setStyle("-fx-background-image: url('/poster_images/seat.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");

                selectedSeat.remove(b.getId());
                //change total price
                summary -= allRound.getCurrentRound().getSeatInTheater().get(b.getId()).getPrice();

            } else {
                //check selected seat
                if (selectedSeat.size() == 10) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("WARNING");
                    alert.setHeaderText(null);
                    alert.setContentText("Limit Exceed");
                    alert.showAndWait();
                } else {
                    selectedSeat.add(b.getId());
                    b.setStyle("-fx-background-image: url('/poster_images/check.png');-fx-background-size: 41 40;-fx-background-position: center center;");
                    summary += allRound.getCurrentRound().getSeatInTheater().get(b.getId()).getPrice();
                }
            }
            labelSummary.setText("" + summary);
            //print all selected seat
            String text = "";
            for (String s : selectedSeat) {
                text += s + "  ";
            }

            labelSelect.setText(text);
        }

        //check if seat booked by current user
        else if (allRound.getCurrentRound().getSeatInTheater().get(b.getId()).getOwner().getUsername().equals(manageAccount.getCurrentCustomers().getUsername())){
            //change seat into no booking seat
            if ((b.getId().charAt(0) == 'A' || b.getId().charAt(0) == 'B' || b.getId().charAt(0) == 'C') && allRound.getCurrentRound().getTheater().getSeatPattern().equals("Mix"))
                b.setStyle("-fx-background-image: url('/poster_images/seatVip.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
            else
                b.setStyle("-fx-background-image: url('/poster_images/seat.png');-fx-background-size: 48 40;-fx-background-color: rgba(255, 255, 255, 0.3);");
            //add seat that can remove in arraylist
            selectedOwnSeat.add(b.getId());
            //can use Remove button
            removeBtn.setDisable(false);
        }

    }

    //Action on MainMenu button
    //back to MainMenu scene
    @FXML
    public void backBtnAction(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/MainMenu.fxml"));

        stage.setScene(new Scene(loader.load(), 1000, 990));


        stage.show();
    }


    //Action on Remove button
    @FXML
    public void removeBtnAction(ActionEvent e){

        //remove owner in seat that selected
        for(String s:selectedOwnSeat){
            allRound.getCurrentRound().getSeatInTheater().get(s).setOwner(null);
        }
        removeBtn.setDisable(true);
        selectedOwnSeat.clear();

        //overwrite file BookingData.csv
        File file = new File("saveFile/BookingData.csv");
        File file1=new File("saveFile/tmp.csv");
        try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            FileReader fileReader = new FileReader(file1);
            BufferedReader reader = new BufferedReader(fileReader);

            //read file from tmp.csv and write on BookingData.csv
            String line ;
            Round r=allRound.getCurrentRound();
            while ((line = reader.readLine()) != null) {
                String[]part=line.split(",");
                //if seat is'n a seat that user want to remove
                //write (copy from temp)
                if((r.getTheater().getName().equals(part[2]) && r.getTime().equals(part[3]) && r.getMovie().getName().equals(part[1]))){
                    for (String key : r.getSeatInTheater().keySet()) {
                        Seat seat = r.getSeatInTheater().get(key);

                        if (seat.getOwner() != null && seat.getName().equals(part[4])) {
                            writer.write(line);
                            writer.newLine();
                        }}
                }
                else {writer.write(line);
                    writer.newLine();}


            }
            reader.close();
            writer.close();
            //make tmp.csv have a same data from BookingData.csv
            Files.copy(Paths.get("saveFile/BookingData.csv"),Paths.get("saveFile/tmp.csv"), StandardCopyOption.COPY_ATTRIBUTES,StandardCopyOption.REPLACE_EXISTING);
        }
         catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    //Action on Buy button
    @FXML
    public void buyBtnAction(ActionEvent e) throws IOException {
        //check that user select available seat at least 1 seat
        if(! selectedSeat.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Confirm your booking", ButtonType.YES, ButtonType.CANCEL);
            alert.setHeaderText(null);
            alert.showAndWait();
            //alert confirm booking
            if (alert.getResult() == ButtonType.YES) {
                    //write booking data in BookingData.csv
                    writeBooking();
                    File file1=new File("saveFile/tmp.csv");
                    file1.createNewFile();
                    //create and make tmp.csv have a same data from BookingData.csv
                    Files.copy(Paths.get("saveFile/BookingData.csv"),Paths.get("saveFile/tmp.csv"), StandardCopyOption.COPY_ATTRIBUTES,StandardCopyOption.REPLACE_EXISTING);
                    //go to next scene (PrintScreen scene) (Bill)
                Button b = (Button) e.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                stage.setResizable(true);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/PrintScreen.fxml"));
                stage.setScene(new Scene(loader.load(), 982, 990));
                //pass data (round,name of all seat and total price that booking)
                PrintScreenController printScreenController=loader.getController();
                printScreenController.passRoundSeat(allRound.getCurrentRound(),labelSelect.getText(),labelSummary.getText());

                stage.show();


            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("WARNING");
            alert.setHeaderText(null);
            alert.setContentText("You haven't selected seat yet");
            alert.showAndWait();

        }

}

    //write file (append next to last line)
    public void writeBooking() {
        File dir = new File("saveFile");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File("saveFile/BookingData.csv");
        try {
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter writer = new BufferedWriter(fileWriter);

            for (String s : selectedSeat) {
                writer.write(manageAccount.getCurrentCustomers().getUsername());
                writer.append(",");
                writer.write(allRound.getCurrentRound().getMovie().getName());
                writer.append(",");
                writer.write(allRound.getCurrentRound().getTheater().getName());
                writer.append(",");
                writer.write(allRound.getCurrentRound().getTime());
                writer.append(",");
                writer.write(allRound.getCurrentRound().getSeatInTheater().get(s).getName());
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Action on Logout button
    //back to login scene
    @FXML
    public void logoutBtnHandle(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/Login.fxml"));
        stage.setScene(new Scene(loader.load(), 982, 990));

        stage.show();
    }
}



