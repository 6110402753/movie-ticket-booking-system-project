package booking_movies_tickets;

import java.time.LocalDate;

//detail of movie
public class Movie {
    private char number;
    private String name;
    private String genre;
    private String description;
    private int length;
    private LocalDate releaseDate;
    private String posterImgPath;
    private String trailerVideoPath;

    public Movie(char number,String name, String genre, int length, LocalDate releaseDate, String posterImgPath,String description,String trailerVideoPath) {
        this.number=number;
        this.name = name;
        this.genre = genre;
        this.description=description;
        this.length = length;
        this.releaseDate = releaseDate;
        this.posterImgPath = posterImgPath;
        this.trailerVideoPath=trailerVideoPath;
    }

    public String getName() {
        return name;
    }


    public String getPosterImgPath() {
        return posterImgPath;
    }

    public String mainToString(){
        return releaseDate+"\n"+name;
    }

    public String getDescription() {
        return description;
    }

    public String getTrailerVideoPath() {
        return trailerVideoPath;
    }

    public char getNumber() {
        return number;
    }
    public String toString(){
        return name+"\n"+"Genre: "+genre+"\n"+"Length: "+length+" Mins";
    }
}
