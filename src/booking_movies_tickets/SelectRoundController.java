package booking_movies_tickets;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import java.io.IOException;



public class SelectRoundController {
    private AllRound allRound;
    private Movie select;
    @FXML
    TableColumn<Round, String> theatercolum;
    @FXML
    TableColumn<Round, String> detail;
    @FXML
    TableColumn<Round, String> timeColumn;
    @FXML
    TableView<Round> table;
    @FXML
    ImageView bigPoster;
    @FXML
    Button selectBtn,trailerBtn;
    @FXML
    Label detailLabel,desLabel;
    @FXML
    AnchorPane anchor;

    private final ObservableList<Round> listItems = FXCollections.observableArrayList();

    //receive movie that user selected from previous scene (Mainmenu)
    void setData(Movie s) {
        this.select=s;
    }

    @FXML
    public void initialize() {
        //set css Stylesheets
        anchor.getStylesheets().add(CssTheme.path);
        //create object from singleton class
        allRound=AllRound.getInstance();

        //set image,text,table from selected movie
        Platform.runLater(() -> {
            //set image from selected movie
            bigPoster.setImage(new Image(select.getPosterImgPath()));
            //set text from selected movie
            desLabel.setText(select.getDescription());
            //set table from selected movie
            for (Round r : allRound.getRound()) {
                if (r.getMovie() == select) {
                    listItems.add(r);
                }
            }
            //set text from selected movie
            detailLabel.setText(select.toString());
        });

        //set table
        theatercolum.setCellValueFactory(new PropertyValueFactory<>("nameTheater"));
        detail.setCellValueFactory(new PropertyValueFactory<>("detail"));
        timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));

        table.setItems(listItems);
    }

    //Action on select button
    @FXML
    public void selectBtnAction(ActionEvent action) throws IOException {

        int selectedItem = table.getSelectionModel().getSelectedIndex();

        //check that user click in table to select item
        if(selectedItem!=-1) {
            //go to next scene(select seat)
            Button b = (Button) action.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/SelectSeat.fxml"));
            Parent root1=(Parent) loader.load();
            stage.setScene(new Scene(root1));
            //round that user select
            allRound.setCurrentRound(listItems.get(selectedItem));
            stage.show();
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Please select round ");

            alert.showAndWait();
        }
    }

    //Action on back button
    //go back to previous scene (main menu)
    @FXML
    public void backBtn(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/MainMenu.fxml"));

        stage.setScene(new Scene(loader.load(), 1000, 990));
        stage.show();
    }

    //Action on trailer button
    //pop up new window and play video that have path in class Movie
    @FXML
    public void trailerBtnhandle(ActionEvent e) {

        MediaPlayer player = new MediaPlayer( new Media(getClass().getResource(select.getTrailerVideoPath()).toExternalForm()));
        MediaView mediaView = new MediaView(player);
        StackPane root = new StackPane();
        root.getChildren().add( mediaView);
        Stage stage = new Stage();
        stage.setTitle(select.getName());
        stage.setScene(new Scene(root,1280,720));
        //trailer button can't click if pop up window is showing
        trailerBtn.setDisable(true);
        stage.show();
        player.play();

        //if close pop up window video will stop and trailer button can click
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                player.stop();
                trailerBtn.setDisable(false);
            }
        });
    }

    //go to login scene to change account
    @FXML
    public void logoutBtnHandle(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/Login.fxml"));
        stage.setScene(new Scene(loader.load(), 982, 990));

        stage.show();
    }




}
