package booking_movies_tickets;

//singleton class that collect all round

import java.util.ArrayList;

public class AllRound {
    private static AllRound AllRound_instance = null;
    private ArrayList<Round> round;

    //round that user select from main menu
    private Round currentRound;

    private AllRound() {
        round=new ArrayList<>();
        currentRound=null;
    }

    public static AllRound getInstance() {
        if(AllRound_instance==null) AllRound_instance=new AllRound();
        return AllRound_instance;
    }

    public void add(Round r){
        round.add(r);
    }

    public ArrayList<Round> getRound() {
        return round;
    }


    public void setCurrentRound(Round currentRound) {
        this.currentRound = currentRound;
    }

    public Round getCurrentRound() {
        return currentRound;
    }
}
