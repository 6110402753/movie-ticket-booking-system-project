package booking_movies_tickets;


public class Theater {
    private String name;
    private String system;
    private String seatPattern;// Ex. mix,general
    private String language;
    private Seat[] typeSeat;

    public Theater(String name, String type, String seatPattern, String language,Seat[] typeSeat) {
        this.seatPattern = seatPattern;
        this.system = type;
        this.name = name;
        this.language = language;
        this.typeSeat=typeSeat;
    }


    public String getName() {
        return name;
    }


    public String getSystem() {
        return system;
    }


    public String getSeatPattern() {
        return seatPattern;
    }

    public String getLanguage() {
        return language;
    }


    public Seat[] getTypeSeats() {
        return typeSeat;
    }



}
