package booking_movies_tickets;


//user's account class

public class Account {

    private String username;
    private String password;
    private String name;
    private String surname;
    private String email;

    public Account(String username, String password,String name,String surname,String email) {
        this.username = username;
        this.password = password;
        this.name=name;
        this.surname=surname;
        this.email=email;
    }


    public boolean checkPassword(String password){
        return this.password.equals(password);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

}
