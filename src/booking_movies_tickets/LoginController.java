package booking_movies_tickets;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LoginController {
    @FXML
    TextField userField;
    @FXML
    PasswordField passField;
    @FXML
    AnchorPane anchor;

    private ManageAccount manageAccount;


    //set Css Stylesheets
    //read file AccountList.csv to create and collect all account
    @FXML
    public void initialize(){
        manageAccount=ManageAccount.getInstance();

        anchor.getStylesheets().add(CssTheme.path);

        if(manageAccount.getCustomers().isEmpty()){
            setUser();
        }
    }


    //Action on button login
    //check username and password
    //if correct both go to next scene(main menu)
    //else show alert box
    @FXML
    public void logInBtn(ActionEvent e) throws IOException {
        String user=userField.getText();
        String pass=passField.getText();
        if(manageAccount.haveAccount(user)){
            if(manageAccount.getCustomers().get(user).checkPassword(pass)){
                manageAccount.setCurrentCustomers(manageAccount.getCustomers().get(user));
                Button b = (Button) e.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
//                stage.setResizable(true);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/MainMenu.fxml"));
                Scene scene=new Scene(loader.load(),1000, 990);

                stage.setScene(scene);
                stage.show();
            }
            else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Error");
                alert.setHeaderText("password is incorrect.");
                alert.setContentText("Please input the correct password again.");
                alert.showAndWait();
            }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("This account does not exist.");
            alert.showAndWait();
        }
    }


    //Action on sign up button
    //go to next scene(sign up)
    @FXML
    public void signUpBtn(ActionEvent e) throws IOException {
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/SignUp.fxml"));
        stage.setScene(new Scene(loader.load(),982, 990));

        stage.show();
    }


    //read file that collect all account
    public void setUser(){
        File file = new File("saveFile/AccountList.csv");
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[]part=line.split(",");
                //part[0]=username  //part[1]=password
                //part[2]=name      //part[3]=surname   //part[4]=e-mail
                Account a =new Account(part[0],part[1],part[2],part[3],part[4]);
                manageAccount.addAccount(a);
            }
            reader.close();}
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    //Action on AboutMe button
    //pop up new window that show scene AboutMe
    public void AboutMeBtn(ActionEvent e) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxmlscene/AboutMe.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("About Me");
        stage.setScene(new Scene(root1));
        stage.show();
    }


    //Action on change theme button
    @FXML
    public void setBtn(ActionEvent e) {
        CssTheme.selectMenu();
        anchor.getStylesheets().clear();
        anchor.getStylesheets().add(CssTheme.path);

    }


}
