package booking_movies_tickets;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.Optional;


//change theme class that chang css files from /CSS

public class CssTheme {
    public static String path= "/CSS/PigeonRouge.css";

    private static void changeTheme(int i){
        switch (i){
            case 1:
                path="/CSS/PurpleTangerineTheme.css";
                break;
            case 2:
                path="/CSS/BluePunchTheme.css";
                break;
            case 3:
                path= "/CSS/PigeonRouge.css";
                break;
        }
    }


    //alert that have button as choice to change theme
    public static void  selectMenu(){
        Alert alert=new Alert(Alert.AlertType.INFORMATION);
        alert.setGraphic(null);
        alert.setTitle("Setting");
        alert.setHeaderText("Select your favorite theme!");

        ButtonType buttonTypeOne = new ButtonType("One");

        ButtonType buttonTypeTwo = new ButtonType("Two");

        ButtonType buttonTypeThree = new ButtonType("Three");

        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree, buttonTypeCancel);

        Button oneBtn = (Button) alert.getDialogPane().lookupButton(alert.getButtonTypes().get(0));
        oneBtn.setStyle("-fx-background-image:  url('/poster_images/background2_1.png'); -fx-text-fill: #ff9184 ;");

        Button twoBtn = (Button) alert.getDialogPane().lookupButton(alert.getButtonTypes().get(1));
        twoBtn.setStyle("-fx-background-image:  url('/poster_images/background1.png'); -fx-text-fill: #ee5b7f ;");

        Button treeBtn = (Button) alert.getDialogPane().lookupButton(alert.getButtonTypes().get(2));
        treeBtn.setStyle("-fx-background-image:  url('/poster_images/background3.png'); -fx-text-fill: white ;");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne){
            changeTheme(1);
        } else if (result.get() == buttonTypeTwo) {
            changeTheme(2); }
        else if (result.get() == buttonTypeThree) {
            changeTheme(3);
        }
    }

}
