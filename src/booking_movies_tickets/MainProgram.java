package booking_movies_tickets;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainProgram extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //start with login scene
        Parent root = FXMLLoader.load(getClass().getResource("/fxmlscene/Login.fxml"));
        primaryStage.setTitle("Booking Movies Tickets-6110402753");
        primaryStage.setResizable(false);
        Scene scene=new Scene(root, 982, 990);
        primaryStage.setScene(scene);

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
