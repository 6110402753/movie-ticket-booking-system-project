package booking_movies_tickets;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;


public class MainMenuController {
    @FXML
    ImageView img1,img2,img3,img4,img5,img6;
    @FXML
    Label name1,name2,name3,name4,name5,name6;
    @FXML
    AnchorPane anchor;

    private AllRound allRound;
    private ManageAccount manageAccount;
    private ArrayList<Movie> movies =new ArrayList<>();
    private Movie selected;

    private Movie movie1,movie2,movie3,movie4,movie5,movie6;


    public void initialize() {
        //set css Stylesheets
        anchor.getStylesheets().add(CssTheme.path);
        //create singleton object
        manageAccount=ManageAccount.getInstance();
        allRound=AllRound.getInstance();

        //create movie,theater,round
       set();
       //set owner in seat that was booked
       setOwner();

       //set name + release date movie
        name1.setText(movie1.mainToString());
        name2.setText(movie2.mainToString());
        name3.setText(movie3.mainToString());
        name4.setText(movie4.mainToString());
        name5.setText(movie5.mainToString());
        name6.setText(movie6.mainToString());

        //set image movie
        img1.setImage(new Image((movie1.getPosterImgPath())));
        img2.setImage(new Image((movie2.getPosterImgPath())));
        img3.setImage(new Image((movie3.getPosterImgPath())));
        img4.setImage(new Image((movie4.getPosterImgPath())));
        img5.setImage(new Image((movie5.getPosterImgPath())));
        img6.setImage(new Image((movie6.getPosterImgPath())));
        name1.setText(movie1.mainToString());

    }

    @FXML public void selectMovie(MouseEvent e) throws IOException {
        ImageView b = (ImageView) e.getSource();

        // select movie that user click its picture
        for(Movie m:movies){
            if(m.getNumber()==(b.getId().charAt(3))){selected=m;}

        }

        //go to next scene (select round)
        Stage stage = (Stage) b.getScene().getWindow();
        stage.setResizable(false);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlscene/SelectRound.fxml"));

        stage.setScene(new Scene(loader.load(),982, 990));

        //sent selected movie to scene2
        SelectRoundController selectRound = loader.getController();
        selectRound.setData(selected);

        stage.show();

    }


    @FXML

    public void set(){
        //create movie
        movie1 = new Movie('1',"Angel Has Fallen", "Action", 120, LocalDate.parse("2019-08-22"), "/poster_images/Angel Has Fallen.jpg",
                "Mike Banning is framed for the attempted assassination of the President and must evade his own agency and the FBI as he tries to uncover the real threat.","/video/Angel Has Fallen.mp4");

        movie2 = new Movie('2',"It Chapter 2", "Horror", 170, LocalDate.parse("2019-09-05"), "/poster_images/It Chapter 2.jpg",
                "Twenty-seven years after their first encounter with the terrifying Pennywise, the Losers Club have grown up and moved away, until a devastating phone call brings them back.","/video/itchapter2.mp4");

        movie3 = new Movie('3',"Weathering with You", "Animation", 115, LocalDate.parse("2019-09-05"), "/poster_images/Weathering with You.jpg",
                "A high-school boy who has run away to Tokyo befriends a girl who appears to be able to manipulate the weather.","/video/Weathering With You.mp4");

        movie4 = new Movie('4',"Farewell Song", " Drama", 115, LocalDate.parse("2019-09-12"), "/poster_images/Farewell Song.jpg",
                "Haru and Reo perform as musical duo Haru-Reo. With their indie music, they suddenly get popular. Even with their newfound success, they decide to disband the duo.","/video/Farewell Song.mp4");

        movie5 = new Movie('5',"Children of the Sea", " Animation", 110, LocalDate.parse("2019-10-10"), "/poster_images/Children of the Sea.jpg",
                "A young girl is drawn into a mystery involving sealife around the world, in which two mysterious boys are somehow involved.","/video/Children of the Sea.mp4");

        movie6 = new Movie('6',"Terminator: Dark Fate", " Action", 107, LocalDate.parse("2019-10-23"), "/poster_images/Terminator Dark Fate.jpg",
                "Sarah Connor and a hybrid cyborg human must protect a young girl from a newly modified liquid Terminator from the future.","/video/TERMINATOR DARK FATE.mp4");
        //-------------------------------------------------------------------------------------------
        //add movie to ArrayList movies
        movies.add(movie1);
        movies.add(movie2);
        movies.add(movie3);
        movies.add(movie4);
        movies.add(movie5);
        movies.add(movie6);
        //----------------------------------------------------------------------------------------------------------

        //creat seat
        Seat regular=new Seat("Regular Seat",180,"/poster_images/seat.png");
        Seat regular3D=new Seat("Regular Seat",240,"/poster_images/seat.png");
        Seat regular4K=new Seat("Regular Seat",300,"/poster_images/seat.png");

//        Seat pair=new Seat("Suite (Pair)",600,"/poster_images/seatPair.png");
        Seat vip=new Seat("VIP seat",200,"/poster_images/seatVip.png");
        Seat vip3D=new Seat("VIP seat",280,"/poster_images/seatVip.png");
        Seat vip4K=new Seat("VIP seat",390,"/poster_images/seatVip.png");


        //add seat to theater
        Seat[]gen={regular};
        Seat[]mix={regular,vip};

        Seat[]gen3D={regular3D};
        Seat[]mix3D={regular3D,vip3D};

        Seat[]gen4K={regular4K};
        Seat[]mix4K={regular4K,vip4K};


//--------------------------------------------------------------------------------------------------------------------
        //creat theater
        Theater theater1 = new Theater("Theater 1", "Digital 2D", "General", "TH/--",gen);
        Theater theater2 = new Theater("Theater 2", "4K", "General", "EN/TH",gen4K);
        Theater theater3 = new Theater("Theater 3", "4K", "Mix", "EN/TH",mix4K);
        Theater theater4 = new Theater("Theater 4", "3D", "Mix", "TH/--",mix3D);

        //----------------------------------------------------
        // round (time) + movie no. + theater no.
        allRound.add(new Round("11:00",movie1, theater1));
        allRound.add(new Round("14:00",movie1, theater2));

        allRound.add(new Round("16:40",movie2, theater1));
        allRound.add(new Round("12:30",movie2, theater3));

        allRound.add(new Round("18:00",movie3, theater2));
        allRound.add(new Round("13:00",movie3, theater4));

        allRound.add(new Round("09:30",movie4,theater3));
        allRound.add(new Round("18:00",movie4,theater4));

        allRound.add(new Round("09:00",movie5,theater1));
        allRound.add(new Round("10:30",movie5,theater4));

        allRound.add(new Round("20:00",movie6,theater2));
        allRound.add(new Round("17:00",movie6,theater3));
    }

    public void setOwner(){
        File file = new File("saveFile/BookingData.csv");
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String line ;
            while ((line = reader.readLine()) != null) {
                String[]part=line.split(",");
                for(Round r:allRound.getRound()){
                    if(r.getTheater().getName().equals(part[2]) && r.getTime().equals(part[3])){
                        r.getSeatInTheater().get(part[4]).setOwner(manageAccount.getCustomers().get(part[0]));
                    }
                }
            }
            reader.close();}
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void setBtn(ActionEvent e) {
        CssTheme.selectMenu();
        anchor.getStylesheets().clear();
        anchor.getStylesheets().add(CssTheme.path);

    }



}
