package booking_movies_tickets;

//singleton class that collect all user's account

import java.util.HashMap;

public class ManageAccount {
    private static ManageAccount instance=null;

    //all user's account
    private HashMap<String,Account> customers;
    //user who login
    private Account currentCustomers;

    private ManageAccount() {

        customers=new HashMap<>();
    }

    public static ManageAccount getInstance() {
        if(instance==null) {
            instance=new ManageAccount();
        }
        return instance;
    }

    public Account getCurrentCustomers() {
        return currentCustomers;
    }

    public void setCurrentCustomers(Account currentCustomers) {
        this.currentCustomers = currentCustomers;
    }

    public void addAccount(Account account){
        customers.put(account.getUsername(),account);
    }

    public boolean haveAccount(String user){
        Account a = customers.get(user);
        return a != null;
    }

    public HashMap<String, Account> getCustomers() {
        return customers;
    }
}
